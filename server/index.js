require('dotenv').config();
const express = require('express');
const consola = require('consola');
const bodyParser = require('body-parser');
const { Nuxt, Builder } = require('nuxt');
// Import and Set Nuxt.js options
const config = require('../nuxt.config.js');
const api = require('./api');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api);

config.dev = process.env.NODE_ENV === 'development';

(async () => {
   // Init Nuxt.js
   const nuxt = new Nuxt(config);

   const PORT = process.env.PORT || 3000;

   // Build only in dev mode
   if (config.dev) {
      const builder = new Builder(nuxt);
      await builder.build();
   } else {
      await nuxt.ready();
   }

   // Give nuxt middleware to express
   app.use(nuxt.render);

   // Listen the server
   app.listen(
      PORT,
      consola.ready({
         message: `Server listening on PORT ${PORT}`,
         badge: true,
      })
   );
})();
