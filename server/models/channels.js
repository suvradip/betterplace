module.exports = (sequelize, DataTypes) =>
   sequelize.define(
      'channels',
      {
         Rank: {
            type: DataTypes.STRING(20),
            allowNull: false,
         },
         Grade: {
            type: DataTypes.STRING(20),
         },
         Name: {
            type: DataTypes.STRING(200),
         },
         Uploads: {
            type: DataTypes.INTEGER(11),
         },
         Subscribers: {
            type: DataTypes.INTEGER(11),
         },
         Views: {
            type: DataTypes.INTEGER(11),
         },
      },
      {
         tableName: 'channels',
      }
   );
