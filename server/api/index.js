const express = require('express');
const router = express.Router();
const db = require('../config/sequelize');
const { Sequelize, channels } = db;

const { Op } = Sequelize;

router.post('/top-yt-list', async (req, res, next) => {
   const { limit = 10, offset = 0 } = req.body;
   const { rows, count } = await channels.findAndCountAll({ limit, offset });
   res.status(200)
      .json({
         count,
         offset,
         limit,
         data: rows,
      })
      .end();
});

router.post('/youtuber', async (req, res, next) => {
   const { id } = req.body;
   const data = await channels.findOne({ where: { id } });
   res.status(200)
      .json(data)
      .end();
});

router.post('/search', async (req, res, next) => {
   const { keywords, limit = 10, offset = 0 } = req.body;
   const { rows, count } = await channels.findAndCountAll({
      where: {
         Name: {
            [Op.like]: `%${keywords}%`,
         },
      },
   });
   res.status(200)
      .json({
         count,
         offset,
         limit,
         data: rows,
      })
      .end();
});

module.exports = router;
