require('dotenv').config();
const fs = require('fs');
const path = require('path');
const csv = require('csv-parser');
const db = require('../config/sequelize');

const { sequelize, channels } = db;
sequelize.sync({
   force: true,
});

async function inserData(data, model) {
   try {
      await model.create(data);
   } catch (error) {
      console.log('data insertion failed', error);
      process.exit();
   }
}

fs.createReadStream(path.resolve('data/channels.csv'))
   .pipe(csv())
   .on('data', async obj => {
      await inserData(obj, channels);
   })
   .on('end', () => {
      console.log('channels data import completed.');
      process.exit();
   });
