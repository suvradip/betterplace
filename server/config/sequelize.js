require('dotenv').config();
const Sequelize = require('sequelize');

const sequelize = new Sequelize(
   process.env.DATABASE_NAME,
   process.env.DATABASE_USERNAME,
   process.env.DATABASE_PASSWORD,
   {
      host: process.env.DATABASE_HOST,
      dialect: 'mysql',
      operatorsAliases: false,
      maxConcurrentQueries: 100,
      logging: false,
      pool: {
         max: 100,
         min: 0,
         acquire: 30000,
         idle: 10000,
      },
   }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Models/tables
db.channels = require('../models/channels')(sequelize, Sequelize);

module.exports = db;
