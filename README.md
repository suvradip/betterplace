# betterPlace

> A full stack web-app for an aggregator of top Youtube channels.

[Repository Link](https://gitlab.com/suvradip/betterplace)

## Project Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# dump data to a databse
$ npm run dbDump
```

> You need to add few enviroment variable to run this project. Please copy the contents of `.env.example` file and create a file `.env` in this project root and assign those varibale values.

### According to tasks followinng things are present

-  [*] Dumping a dataset to MySQL database and creating the required schema.
-  [*] Implemented a feature to search channels by name. Provide different filters based on other params as well.
-  [*] Specific page with channel details on the frontend.
-  [*] Implemented paging ​to display the results properly.

### Project details

#### Stack using

-  Frontend - HTML, CSS, Nuxt
-  Backend - Node, Express, Sequelize
-  Db - Mysql

#### Api endpoints

1. Seraching via channel name `/api/search`

   Method: POST

   Params:

   -  keywords &lt;String&gt;
   -  limit &lt;Integer&gt;
   -  offset &lt;Integer&gt;

Example

```bash
curl -X POST \
  http://localhost:3000/api/search \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 57843843-d3cd-44fa-8c62-7f45062b24b2' \
  -H 'cache-control: no-cache' \
  -d '{
    "keywords": "zee"
}'
```

2. Get list of channels `/api/top-yt-list`

   Method: POST

   Params:

   -  limit &lt;Integer&gt;
   -  offset &lt;Integer&gt;

Example

```bash
curl -X POST \
  http://localhost:3000/api/top-yt-list \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 3658190e-ab81-4075-9bf8-467dadf5487c' \
  -H 'cache-control: no-cache' \
  -d '{
    "keywords": "zee"
}'
```

3. Get details view `/api/youtuber`

   Method: POST

   Params:

   -  id &lt;Integer&gt;

```bash
curl -X POST \
  http://localhost:3000/api/youtuber \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 3453a72e-3035-470d-bea3-e46ca52af7a5' \
  -H 'cache-control: no-cache' \
  -d '{
    "id": 12
}'
```
