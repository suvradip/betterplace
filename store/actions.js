export default {
   async FETCH_YT_CHANNELS({ commit }, payload) {
      const data = {
         ...(payload.limit && { limit: payload.limit }),
         ...(payload.offset && { offset: payload.offset }),
      };
      const response = await this.$axios.post('/top-yt-list', data);
      commit('SET_TOP_YT_LIST', response.data);
   },

   async FETCH_YT_DETAILS({ commit }, { id }) {
      const response = await this.$axios.post('/youtuber', { id });
      commit('SET_YT_DETAILS', response.data);
   },

   async FETCH_SEARCH_QUERY({ commit }, payload) {
      const data = {
         ...(payload.limit && { limit: payload.limit }),
         ...(payload.offset && { offset: payload.offset }),
         ...(payload.words && { keywords: payload.words }),
      };
      const response = await this.$axios.post('/search', data);
      commit('SET_TOP_YT_LIST', response.data);
   },
};
