import Vue from 'vue';

export default {
   SET_TOP_YT_LIST: (state, data) => {
      Vue.set(state, 'topYTList', data);
   },

   SET_YT_DETAILS: (state, data) => {
      Vue.set(state, 'youtuber', data);
   },
};
